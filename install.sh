#!/bin/sh
# Simple install script which runs makefiles
# of every package separately.

echo "Installing rescaffolder package version 1.0.0.[comp]"
echo "Moving to source directory."
cd src
echo "Installing rescaffolder connected component version..."
echo "Compiling rescafolder..."
make
echo "Cleaning source directory.."
rm -rf src/*.o
echo "Done."
cd gapEstimater
echo "Installing gap estimator..."
make
echo "Done."
cd ..
mv rescaffolder ..
echo "Creating output directory..."
cd ..
mkdir output
echo "Done."

