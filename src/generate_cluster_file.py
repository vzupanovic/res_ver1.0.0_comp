import sys
import sys
import os

def constructEdgeFile(fileName):
        pass

def main():
        if len(sys.argv) < 2:
                print "Please provide at least one edge file."
                exit(-1)
        fileNum = len(sys.argv)
        catString = ''
        dest = "/mnt/ScratchPool/gisv61/ver2_copy_final/src/datasets/drosophila_20k_200bp/all_edges.cluster"
        os.system('echo "First Contig   Orientation     Second Contig   Orientation     Distance        Standard Deviation      Size" > ' + dest)
        for i in range(1, fileNum):
                catString = catString + sys.argv[i] + " "
        os.system('cat ' + catString + '| sort | uniq -u | grep -v "First Contig" >> '+dest)

main()

