#ifndef EDGE_LOADER_H
#define EDGE_LOADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

/*
 * Class used to load edge data into simple data strucures
 * which will be used later for construction of scaffoldGraph.
 * Class also contains the method for filtering edges that are
 * supported by less then N reads.
 * */

class EdgeLoader{
	public:
	
	map<string, vector<string> > loadContigDataToMap(const char * fileName){
		map<string, vector<string> > contigData;
		string contigInfo;
		vector<string> data;
		
		ifstream inFile(fileName);
		if (!inFile){
			cerr << "Can't open scaffold file, exiting..." << endl;
			exit(1);
		}
		string header; //skip header line
		getline(inFile, header);
		while (getline(inFile, contigInfo)){
			vector<string> tokens;
			boost::algorithm::split(tokens, contigInfo, boost::algorithm::is_any_of("\t"));
			string contigName, contigOrientation, contigLength, gapSize;
			if (contigInfo[0] != '>'){
				data.push_back(tokens[1]);
				data.push_back(tokens[2]);
				data.push_back(tokens[3]);
				//cout << "data size: " << data.size() << " " << data[0] << data[1] << data[2] << endl;
				contigData.insert(make_pair(tokens[0], data));
				data.clear();
			}
	
		}
		return contigData;
	}
	
	void getEdgesFromClusterFile(const char * name, vector<vector<string> >& edgeData){
		vector<string> edge;
		string line;
		edge.reserve(7);
		ifstream inFile(name);
		if (!inFile){
			cerr << "Can't open cluster file, exiting..." << endl;
			exit(1);
		}
		string tableInfo;
		getline(inFile, tableInfo);
		while (getline(inFile, line)){
			vector<string> tokens;
			boost::algorithm::split(tokens, line, boost::algorithm::is_any_of("\t"));
			edge.push_back(tokens[0]);
			edge.push_back(tokens[1]);
			edge.push_back(tokens[2]);
			edge.push_back(tokens[3]);
			edge.push_back(tokens[4]);
			edge.push_back(tokens[5]);
			edge.push_back(tokens[6]);
			edgeData.push_back(edge);
			edge.clear();
		}
	}
	
	vector<vector<string> > filterLessSupportedEdges(int numberOfReads, vector<vector<string> > edgeData){
		vector<vector<string> > copyEdgeData;
		for (int i = 0; i < edgeData.size(); i++){
			int num = boost::lexical_cast<int>(edgeData[i][6]);
			if (num >= numberOfReads){
				copyEdgeData.push_back(edgeData[i]);
			}
		}
		copyEdgeData.reserve(copyEdgeData.size());
		return copyEdgeData;
	}
	
};

#endif /*EDGE_LOADER_H*/
