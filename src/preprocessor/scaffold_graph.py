# simple module used to update edges inside the scaffold graph
# structure after the creation of super-nodes is finished.
# Scaffold graph is stored as a large dictionary which enables
# to do simple graph contraction efficiently. 
# Module removes all the unecessary edges which are connecting
# nodes inside the same super-node and updates all the information
# about the current edge like distance, orientation and gap size. 

import numpy as np
import math
import sys
import os.path
from loader import *

class ScaffoldGraph:
	def __init__(self, contigInfo, scaffoldData, edgeData): #contigData - dict, edgeData - list, scaffoldData - dict
		self.contigInfo = contigInfo
		self.scaffoldData = scaffoldData
		self.edgeData = edgeData
		self.updatedEdgesFile = open("src/preprocessor/all_edges.updated.cluster", "w")
		self.updatedEdgesFile.write("First Contig\tOrientation\tSecond Contig\tOrientation\tDistance\tStandard Deviation\tSize\n")
		self.scaffoldGraph = {} #initialize empty dict for storing graph like structure

		for edge in self.edgeData: #first create empty adjency list for every node (contig)
			self.scaffoldGraph[edge[0]] = ({},{}) #first dict - in edges, second dict - out egdes
			
		for edge in self.edgeData:
				self.scaffoldGraph[edge[2]] = ({},{})
		
		for edge in self.edgeData: #collect all the left nodes
			wantedNode = edge[0]
			if edge[1] == '-': #in edge
				self.scaffoldGraph[wantedNode][0][edge[2]] = [edge[1], edge[3], 'START', 'END', edge[4], edge[5], edge[6]]
			else: #out edge
				self.scaffoldGraph[wantedNode][1][edge[2]] = [edge[1], edge[3], 'START', 'END', edge[4], edge[5], edge[6]]
			
		for edge in self.edgeData: #insert all the right nodes
			wantedNode = edge[2]
			if wantedNode in self.scaffoldGraph:
				if edge[3] == '+': #in edge 
					self.scaffoldGraph[wantedNode][0][edge[0]] = [edge[3], edge[1], 'END', 'START', edge[4], edge[5], edge[6]]
				else: #out edge
					self.scaffoldGraph[wantedNode][1][edge[0]] = [edge[3], edge[1], 'END', 'START', edge[4], edge[5], edge[6]]

		
	def reconstructEdge(self, vertex1, vertex2):
		data = self.scaffoldGraph[vertex1]
		if vertex2 in data[0]:
			edge = data[0][vertex2]
			if edge[2] == "START":
				#print vertex1,"\t",edge[0],"\t",vertex2,"\t",edge[1],"\t",edge[4],"\t",edge[5],"\t",edge[6]
				if vertex1 != vertex2:
					self.updatedEdgesFile.write(vertex1+"\t"+edge[0]+"\t"+vertex2+"\t"+edge[1]+"\t"+str(edge[4])+"\t"+str(edge[5])+"\t"+str(edge[6])+"\n")
			else:
				#print vertex2,"\t",edge[1],"\t",vertex1,"\t",edge[0],"\t",edge[4],"\t",edge[5],"\t",edge[6]
				if vertex1 != vertex2:
					self.updatedEdgesFile.write(vertex2+"\t"+edge[1]+"\t"+vertex1+"\t"+edge[0]+"\t"+str(edge[4])+"\t"+str(edge[5])+"\t"+str(edge[6])+"\n")

				
		elif vertex2 in data[1]:
			edge = data[1][vertex2]
			if edge[2] == "START":
				#print vertex1,"\t",edge[0],"\t",vertex2,"\t",edge[1],"\t",edge[4],"\t",edge[5],"\t",edge[6]
				if vertex1 != vertex2:
					self.updatedEdgesFile.write(vertex1+"\t"+edge[0]+"\t"+vertex2+"\t"+edge[1]+"\t"+str(edge[4])+"\t"+str(edge[5])+"\t"+str(edge[6])+"\n")
			else:
				#print vertex2,"\t",edge[1],"\t",vertex1,"\t",edge[0],"\t",edge[4],"\t",edge[5],"\t",edge[6]
				if vertex1 != vertex2:
					self.updatedEdgesFile.write(vertex2+"\t"+edge[1]+"\t"+vertex1+"\t"+edge[0]+"\t"+str(edge[4])+"\t"+str(edge[5])+"\t"+str(edge[6])+"\n")
		

	def updateLinkToOldNode(self, linkedNodes, oldContigKey, newContigKey, position, rightDistance, leftDistance):
		if oldContigKey in linkedNodes:
			#if node == oldContigKey:
			linkedNodes[newContigKey] = linkedNodes.pop(oldContigKey)
			data = linkedNodes[newContigKey]
			self.updateOrientationOnEdge(data, position, oldContigKey)
			self.updateDistanceOnEdge(data, position, oldContigKey, rightDistance, leftDistance)

	
	def updateDistanceOnEdge(self, data, position, oldContigKey, rightDistance, leftDistance):
		initialOrientationInEdge = '+'
		position = "START"
		direction = 0 #0 - left edge 1 - right edge
		if position:
			initialOrientationInEdge = data[1]
			position = "END"
		else:
			initialOrientationInEdge = data[0]
			position = "START"
		orientationInSuperContig = self.contigInfo[oldContigKey][0]
		if orientationInSuperContig == '+':
			if position == "START":
				if initialOrientationInEdge == '+':
					direction = 1
				else:
					direction = 0
			else:
				if initialOrientationInEdge == '+':
					direction = 0
				else:
					direction = 1
		else:
			if position == "START":
				if initialOrientationInEdge == '+':
					direction = 0
				else:
					direction = 1
			else:
				if initialOrientationInEdge == '+':
					direction = 1
				else:
					direction = 0
		if direction == 0: #left edge 
			#temp = list(data)
			previous = int(data[4])
			data[4] = str(previous - leftDistance)
			#temp[5] = str(size)
			#data = tuple(temp)
		else:
			#temp = list(data)
			previous = int(data[4])
			data[4] = str(previous - rightDistance)
			#temp[5] = str(size)
			#data = tuple(temp)
		#print data
			
	def updateOrientationOnEdge(self, data, position, oldContigKey):
		orientationInSuperContig = self.contigInfo[oldContigKey][0]
		if position:
			if data[1] == '+' and orientationInSuperContig == "BE":
				pass
			elif data[1] == '-' and orientationInSuperContig == "EB":
				#temp = list(data)
				data[1] = '+'
				#data = tuple(temp)
			else:
				#temp = list(data)
				data[1] = '-'
				#data = tuple(temp)
		else:
			if data[0] == '+' and orientationInSuperContig == "BE":
				pass
			elif data[0] == '-' and orientationInSuperContig == "EB":
				#temp = list(data)
				data[0] = '+'
				#data = tuple(temp)
			else:
				#temp = list(data)
				data[0] = '-'
				#data = tuple(temp)
	
		#print data
	
	
	def updateEdge(self, oldContigKey, newContigKey, rightDistance, leftDistance):
		#self.scaffoldGraph[newContigKey] = self.scaffoldGraph.pop(oldContigKey)
		data = self.scaffoldGraph[oldContigKey]
		previous = data[0]
		next = data[1]
		self.scaffoldGraph[newContigKey] = self.scaffoldGraph.pop(oldContigKey)
		for linkedEdge in data[0]:
			dataOnEdge = data[0][linkedEdge]
			self.updateOrientationOnEdge(dataOnEdge, 0, oldContigKey)
			self.updateDistanceOnEdge(dataOnEdge, 0, oldContigKey, rightDistance, leftDistance)
			dataOfInner = self.scaffoldGraph[linkedEdge]
			self.updateLinkToOldNode(dataOfInner[0], oldContigKey, newContigKey, 1, rightDistance, leftDistance)
			self.updateLinkToOldNode(dataOfInner[1], oldContigKey, newContigKey, 1, rightDistance, leftDistance)
		for linkedEdge in data[1]:
			dataOnEdge = data[1][linkedEdge]
			self.updateOrientationOnEdge(dataOnEdge, 0, oldContigKey)
			self.updateDistanceOnEdge(dataOnEdge, 0, oldContigKey, rightDistance, leftDistance)
			#a = raw_input("jao")
			dataOfInner = self.scaffoldGraph[linkedEdge]
			self.updateLinkToOldNode(dataOfInner[0], oldContigKey, newContigKey, 1, rightDistance, leftDistance)
			self.updateLinkToOldNode(dataOfInner[1], oldContigKey, newContigKey, 1, rightDistance, leftDistance)		

	def reconstructEdgeFile(self):
		for contig in self.scaffoldGraph:
			data = self.scaffoldGraph[contig]
			for linkedContig in data[0]:
				self.reconstructEdge(contig, linkedContig)
			for linkedContig in data[1]:
				self.reconstructEdge(contig, linkedContig)
	
	def getDistance(self, firstContig, secondContig): #get distance between contigs (edge), this is mean!
		distancePrevious = 0
		distanceNext = 0
		if firstContig in self.scaffoldGraph:
			dataPrevious = self.scaffoldGraph[firstContig][0]
			dataNext = self.scaffoldGraph[firstContig][1]
			if secondContig in dataPrevious:
				distancePrevious = dataPrevious[secondContig][0]
			if secondContig in dataNext:
				distanceNext = dataNext[secondContig][0]
		return (distancePrevious, distanceNext)
		
		
	def getDeviation(self, firstContig, secondContig): #get standard deviation of the edge
		deviationPrevious = 0
		deviationNext = 0
		if firstContig in self.scaffoldGraph:
			dataPrevious = self.scaffoldGraph[firstContig][0]
			dataNext = self.scaffoldGraph[firstContig][1]
			if secondContig in dataPrevious:
				deviationPrevious = dataPrevious[secondContig][1]
			if secondContig in dataNext:
				deviationNext = dataNext[secondContig][1]
		return (deviationPrevious, deviationNext)
		
	def checkConnectivity(self, contig1, contig2):
		#var = raw_input("dalje")
		if contig1 in self.scaffoldGraph:
			if ((contig2 in self.scaffoldGraph[contig1][0]) or (contig2 in self.scaffoldGraph[contig1][1])):
				return True
		return False
		
	def checkConnectivityComponent(self, outerContig, connectedComponent, coreContig):
		#var = raw_input("dalje")
		if self.checkConnectivity(outerContig, coreContig):
			#print "DODAJEM ", outerContig
			return True
		for contig in connectedComponent:
			if self.checkConnectivity(contig, outerContig):
				#print "DODAJEM ", outerContig
				return True
		#print "NE DODEAJM", outerContig
		return False
	
		
	def getSize(self, firstContig, secondContig): #get distance between two border contigs in the edge
		sizePrevious = 0
		sizeNext = 0
		if firstContig in self.scaffoldGraph:
			dataPrevious = self.scaffoldGraph[firstContig][0]
			dataNext = self.scaffoldGraph[firstContig][1]
			if secondContig in dataPrevious:
				sizePrevious = dataPrevious[secondContig][2]
			if secondContig in dataNext:
				sizeNext = dataNext[secondContig][2]
		return (sizePrevious, sizeNext)
			
		
	def getEdges(self, contig): #return edge data by key contig
		return self.scaffoldGraph[contig]
		
	
	def getAllPaths(self, start, end, direction = 1, path = []): #initial direction is 1 -> look out edges
		path = path + [start]
		if start == end:
			return [path]
		if not self.scaffoldGraph.has_key(start):
			return []
		paths = []
		for node in self.scaffoldGraph[start][direction]:
			direction = direction ^ 1
			if node not in path:
				newPaths = self.getAllPaths(node, end, direction, path)
				if not newPaths:
					pass
				else:
					for newPath in newPaths:
						paths.append(newPath)
		return paths
		
		
	def getShortestPath(self, start, end): #get smallest path in between two contigs
		allPaths = self.getAllPaths(start, end)
		if not allPaths:
			return None
		smallestPath = allPaths[0]
		for path in allPaths:
			if len(path) < len(smallestPath):
				smallestPath = path
		return smallestPath
		
	
	def getNextNodes(self, node): #get next nodes
		return self.scaffoldGraph[node][1]
		
		
	def getPreviousNodes(self, node): #get previous nodes
		return self.scaffoldGraph[node][0]
		
	def replaceContigWithSupercontig(self, oldKeyContig, newKeyContig):
		data = self.scaffoldGraph[oldKeyContig]
		self.scaffoldGraph[newKeyContig] = self.scaffoldGraph.pop(oldKeyContig)
		data = self.scaffoldGraph[newKeyContig]
			
	
	
