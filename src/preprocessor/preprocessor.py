# Main module used for creating super-nodes from input
# scaffold and cluster file. Module is invoked by rescaffold.cpp.
# Preprocessor module contains every method necessary to
# create super-nodes and uses pickle module to store all 
# the information about merged regions inside the *.p files.


import math
import sys
import os
from loader import *
from scaffold_graph import *
from simple_edge_graph import *
from Queue import PriorityQueue

class Preprocessor:
	def __init__(self, clusterFile, contigFile, scaffoldFile, threshold):
		
		self.visitedNodes = {} #dictionary of visited scaffolds
		self.acceptedScaffolds = [] #list of nodes, each node is scaffold
		self.openNodes = []
		print "Creating supercontigs with threshold: ", threshold	
		loader = Loader(clusterFile, contigFile, scaffoldFile)
		self.edgeData = loader.loadClusterFile()
		self.contigData = loader.loadContigFile()
		self.scaffoldData, self.orderedContigsList, self.origin, self.contigInfo, self.extractedScaffolds = loader.loadScaffoldFile()
		self.mergedScaffoldFile = open("src/preprocessor/scaffolds_merged.scaf", 'w')
		
		self.filterUnnecessaryEdges(5) #remove edges with less then 5 reads that supports them
		
		self.scaffoldGraphObject = ScaffoldGraph(self.contigInfo, self.scaffoldData, self.edgeData) #define scaffold graph structure
		self.firstRun(threshold)
		self.mergedScaffoldFile.close()
		print "Creating updated scaffold file..."
		self.reconstructEdgeFile()
		print "Updating edges..."
		print "Done."
		
	def filterUnnecessaryEdges(self, size): #filter all edges with the size less then parameter size (5 in current impl.)
		validEdges = []
		for edge in self.edgeData:
			if (int(edge[-1]) >= size): #check if size parameter is less then 5
				validEdges.append(edge)
		self.edgeData = validEdges[:]
		
	def firstRun(self, treshold):
		scaffold_id = 1
		self.mergeRegions = {}
		for scaffold in self.extractedScaffolds:
			self.mergedScaffoldFile.write(">opera_scaffold_"+str(scaffold_id)+"\n")
			self.defineCloseRegions(treshold, scaffold)
			scaffold_id = scaffold_id + 1
		pickle.dump(self.mergeRegions, open("src/preprocessor/regions.info.p", "wb"))
			
	def getInnerGap(self, contigData):
		return int(contigData[3])
		
	def checkConnectivity(self, contig1, contig2):
		return self.scaffoldGraphObject.checkConnectivity(contig1, contig2)
		
	def checkConnectivityComponent(self, outerContig, connectedComponent, coreContig):
		return self.scaffoldGraphObject.checkConnectivityComponent(outerContig, connectedComponent, coreContig)
		
	def getGapSize(self, coreContig, connectedComponent):
		return self.contigInfo[connectedComponent[-1]][2]
		
	def calculateLen(self, coreContig, connectedComponent):
		initialLen = int(self.contigInfo[coreContig][1]) + int(self.contigInfo[coreContig][2])
		for i in range(0, len(connectedComponent) -1):
			initialLen += int(self.contigInfo[connectedComponent[i]][1]) + int(self.contigInfo[connectedComponent[i]][2])
		initialLen += int(self.contigInfo[connectedComponent[-1]][1])
		return initialLen	
		
	def writeSinlgeton(self, coreContig):
		self.mergedScaffoldFile.write(coreContig + "\t" + str(self.contigInfo[coreContig][0]) + "\t" + str(self.contigInfo[coreContig][1]) + "\t" + str(self.contigInfo[coreContig][2]) + "\n")
		
	def writeNewRegionToFile(self, coreContig, connectedComponent):
		if connectedComponent:
			superContigLen = self.calculateLen(coreContig, connectedComponent)
			gapSize = self.getGapSize(coreContig, connectedComponent)
			self.mergedScaffoldFile.write(coreContig+ "M" + "\tBE\t"+str(superContigLen)+"\t"+str(gapSize)+"\n")
		else:
			self.mergedScaffoldFile.write(coreContig + "\t" + str(self.contigInfo[coreContig][0]) + "\t" + str(self.contigInfo[coreContig][1]) + "\t" + str(self.contigInfo[coreContig][2]) + "\n")

	def getRightDistance(self, mergedRegion, innerContig, position):
		rightDistance = int(self.contigInfo[innerContig][2])
		for i in range(position + 1, len(mergedRegion) -1):
			currentContig = mergedRegion[i]
			rightDistance += int(self.contigInfo[currentContig][1]) + int(self.contigInfo[currentContig][2])
		lastContig = mergedRegion[-1]
		rightDistance += int(self.contigInfo[lastContig][1])
		return rightDistance
	
	def getLeftDistance(self,  mergedRegion, innerContig, position):
		leftDistance = 0
		for i in range(0, position):
			currentContig = mergedRegion[i]
			leftDistance += int(self.contigInfo[currentContig][1]) + int(self.contigInfo[currentContig][2])
		return leftDistance

	def updateEdges(self, regionCentre, region):
		if region:
			mergedRegion = [regionCentre] + region
			iterator = 0
			for contig in mergedRegion:
				rightDistance = self.getRightDistance(mergedRegion, contig, iterator)
				leftDistance = self.getLeftDistance(mergedRegion, contig, iterator)
				#print rightDistance, leftDistance, contig, mergedRegion
				self.scaffoldGraphObject.updateEdge(contig, regionCentre+"M", rightDistance, leftDistance)
				iterator = iterator + 1

			
	def defineCloseRegions(self, treshold, scaffold):
		self.mergeRegions[scaffold[0][0]] = []
		regionCentre = scaffold[0][0]
		for i in range(1, len(scaffold)):
			if (self.getInnerGap(scaffold[i - 1]) <= treshold and self.checkConnectivityComponent(scaffold[i][0], self.mergeRegions[regionCentre], regionCentre)):
				self.mergeRegions[regionCentre].append(scaffold[i][0])
			else:
				self.writeNewRegionToFile(regionCentre, self.mergeRegions[regionCentre])
				self.updateEdges(regionCentre, self.mergeRegions[regionCentre])
				regionCentre = scaffold[i][0]
				self.mergeRegions[regionCentre] = []
		self.updateEdges(regionCentre, self.mergeRegions[regionCentre])
		self.writeNewRegionToFile(regionCentre, self.mergeRegions[regionCentre])


	def reconstructEdgeFile(self):
		self.scaffoldGraphObject.reconstructEdgeFile()

if  __name__ == "__main__":
	if (len(sys.argv) < 5):
		print "Not enough arguments provided, exiting..."
		exit(-1)
	cluster_file = sys.argv[1]
	contig_file = sys.argv[2]
	scaffold_file = sys.argv[3]
	thrsh = int(sys.argv[4])
	if os.path.isfile(cluster_file)!=True:
		print "Cluster file doesn't exist..."
		exit(-1)
	if os.path.isfile(contig_file)!=True:
		print "Contig file doesn't exist..."
		exit(-1)
	if os.path.isfile(scaffold_file) != True:
		print "Scaffold file dosen't exist..."
		exit(-1)
	if (thrsh < -41):
		print "Wrong number for threshold, setting it to 400 - default."
		thrsh = 400
	preprocessor = Preprocessor(cluster_file, contig_file, scaffold_file, thrsh)
	
