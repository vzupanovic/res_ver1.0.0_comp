#ifndef SCAFFOLD_GRAPH_H
#define SCAFFOLD_GRAPH_H

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>
#include <unordered_map>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>
#include <stdbool.h>

#include "edge_loader.h"
#include "inner_types.h"

using namespace std;

/*
 * ScaffoldGraph - class used for storing scaffoldGraph structure,
 * the large map with all the edges gotten from the cluster file.
 * It contains methods for getting the vector of contigs 
 * linked by any node and all the data stored inside any 
 * edge.
 * */

class ScaffoldGraph{
	
	public:
	unordered_map<string, vectorData> scaffoldGraph;
	ScaffoldGraph(map<string, vector<string> > contigData, vector<vector<string> > edgeData){
	map<string, vector<string> >::iterator pozInner;
	map<string, vector<string> >::iterator pozOuter;
	cout << "Constructing scaffold graph..." << endl;
	for (int i = 0; i < edgeData.size(); i++){
		InnerMap previousEdges;
		InnerMap nextEdges;
		scaffoldGraph[edgeData[i][0]].push_back(previousEdges);
		scaffoldGraph[edgeData[i][0]].push_back(nextEdges);
		scaffoldGraph[edgeData[i][2]].push_back(previousEdges);
		scaffoldGraph[edgeData[i][2]].push_back(nextEdges);
	}
	for (int i = 0; i < edgeData.size(); i++){
		InnerMap incomingEdge;
		InnerMap outcomingEdge;
		string outerKeyNode = edgeData[i][0];
		string innerKeyNode = edgeData[i][2];
		pozInner = contigData.find(innerKeyNode);
		pozOuter = contigData.find(outerKeyNode);
		vector<string> data;
		data.push_back(edgeData[i][4]);
		data.push_back(edgeData[i][5]);
		data.push_back(edgeData[i][6]);
		data.push_back(edgeData[i][1]);
		data.push_back(edgeData[i][3]);

		if (edgeData[i][1] == "-"){
	
			scaffoldGraph[edgeData[i][0]][0][innerKeyNode] = data;
		}
		else {
			scaffoldGraph[edgeData[i][0]][1][innerKeyNode] = data; 
		}
		
		if (edgeData[i][3] == "+"){
			scaffoldGraph[edgeData[i][2]][0][outerKeyNode] = data; 
		}
		else{
			scaffoldGraph[edgeData[i][2]][1][outerKeyNode] = data; 
		}
		data.clear();
	}
	}
	void printOutScaffoldGraph(){
		unordered_map<string, vectorData>::iterator poz;
		for (poz = scaffoldGraph.begin(); poz != scaffoldGraph.end(); poz++){
			InnerMap::iterator innerPozPrevious;
			InnerMap::iterator innerPozNext;
			cout << "CURRENT NODE:" << poz->first << endl;
			vectorData tempVector = poz->second;
			InnerMap previousNodes;
			InnerMap nextNodes;
			previousNodes = tempVector[0];
			nextNodes = tempVector[1];
			for (innerPozPrevious = previousNodes.begin(); innerPozPrevious != previousNodes.end(); innerPozPrevious++){
				vector<string> temp = innerPozPrevious -> second;
				cout << "Previous nodes: " << innerPozPrevious -> first << " ";
				cout << "data: " << (innerPozPrevious -> second)[0] << " " << (innerPozPrevious -> second)[1] << " " << (innerPozPrevious -> second)[2] << endl;
				 
			}
			for (innerPozNext = nextNodes.begin(); innerPozNext != nextNodes.end(); innerPozNext++){
				vector<string> temp = innerPozNext -> second;
				cout << "Next nodes: " << innerPozNext -> first << " ";
				cout << "data: " << (innerPozNext -> second)[0] << " " << (innerPozNext -> second)[1] << " " << (innerPozNext -> second)[2] << endl;
				 
			}
			cout << endl;
			
		}
	}
	
	int getEdgeData(string contig1, string contig2, int parameter = 0){
		unordered_map<string, vectorData>::iterator poz1;
		poz1 = scaffoldGraph.find(contig1);
		if (poz1 != scaffoldGraph.end()){
			vectorData temp1 = poz1 -> second;
			InnerMap::iterator innerContigPoz;
			InnerMap previousContigs = temp1[0];
			InnerMap nextContigs = temp1[1];
			innerContigPoz = previousContigs.find(contig2);
			if (innerContigPoz != previousContigs.end()){
				vector<string> data = innerContigPoz -> second;
				return  boost::lexical_cast<int>(data[parameter]);
			}
			innerContigPoz = nextContigs.find(contig2);
			if (innerContigPoz != nextContigs.end()){
				vector<string> data = innerContigPoz -> second;
				return  boost::lexical_cast<int>(data[parameter]);
			}
			return 0; //edge doesn't exists
		}
		return 0;
	}
	
	bool checkIfContigIsVertex(string contigName){
		if (scaffoldGraph.find(contigName) == scaffoldGraph.end())
			return false;
		return true;
	}
	
	vector<string> getContigPairs(string contigName){
		vector<string> contigPairs;
		unordered_map<string, vectorData>::iterator poz;
		poz = scaffoldGraph.find(contigName);
		InnerMap::iterator innerContigPoz;
		if (poz != scaffoldGraph.end()){
			for (innerContigPoz = (poz->second)[0].begin(); innerContigPoz != (poz->second)[0].end(); innerContigPoz ++){
				contigPairs.push_back(innerContigPoz -> first);
			}
			for (innerContigPoz = (poz->second)[1].begin(); innerContigPoz != (poz->second)[1].end(); innerContigPoz ++){
				contigPairs.push_back(innerContigPoz -> first);
			}
		}
		return contigPairs;
		
	}
	
	int orientationMapper(string orientation){
		string plus_plus = "++";
		string plus_minus = "+-";
		string minus_plus = "-+";
		string minus_minus = "--";
		
		if (orientation == plus_plus)
			return 3;
		else if (orientation == plus_minus)
			return 2;
		else if (orientation == minus_plus)
			return 1;
		else if (orientation == minus_minus)
			return 0;
		return -1;
	}
	
	int getOrientation(string contig1, string contig2){
		unordered_map<string, vectorData>::iterator poz1;
		poz1 = scaffoldGraph.find(contig1);
		if (poz1 != scaffoldGraph.end()){
			vectorData temp1 = poz1 -> second;
			InnerMap::iterator innerContigPoz;
			InnerMap previousContigs = temp1[0];
			InnerMap nextContigs = temp1[1];
			innerContigPoz = previousContigs.find(contig2);
			if (innerContigPoz != previousContigs.end()){
				vector<string> data = innerContigPoz -> second;
				//cout << "Orijentacije u edgu: " << contig1 << " " << contig2 << data[3] << " " << data[4] << endl;
				return orientationMapper(data[3] + data[4]);
			}
			innerContigPoz = nextContigs.find(contig2);
			if (innerContigPoz != nextContigs.end()){
				vector<string> data = innerContigPoz -> second;
				//cout << "Orijentacije u edgu: " << contig1 << " " << contig2 << data[3] << " " << data[4] << endl;
				return orientationMapper(data[3] + data[4]);
			}
			return 0; //edge doesn't exists
		}
		return 0;
	}
	
	

};

#endif /*LOADER_H*/

