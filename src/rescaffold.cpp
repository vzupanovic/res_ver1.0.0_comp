#include <iomanip>

#include "local_search.h"

/*
 * rescaffolder.cpp - core module invoked by main.cpp
 * It initializes localSearch class, and calls preprocessor
 * module if parameter t is specified. It also outputs
 * all the necessary statistics connected with partial scaffold
 * processing like percentage of length removed per scaffold,
 * contigs deleted from ambiguous region, etc. 
 * */

ofstream outputScaffFile;
ifstream inputScaffFile;
ofstream basicStats;
ofstream fastaOutput;
ifstream contigFile;
unordered_map<int, vector<int> > scaffoldInfo;
unordered_map<int, vector<int> >::iterator pos;
unordered_map<string, string> contigMapper;
long long int removedTotal;
long long int lengthTotalGlobal;
long long int removedTotalGlobal;

/*
 * Function used to get all stats connected with partial scaffold
 * processing - initial partial scaffold length, number of contigs removed
 * from scaffold, remained length, etc. Statistics for every partial scaffold
 * are outputed inside initial_stats.txt file and basic_stats.txt file.
 * */

void getStatistics(LocalSearch * localSearch){
	basicStats.open("src/basic_stats.txt");
	removedTotal = 0;
	cout << "Outputing statistics..." << endl;
	cout << "Old scaff. ID:  | Num. of removed: | Remained length: | Initial length: | Length fraction: " << endl;
	basicStats << "Old scaff. ID:  | Num. of removed: | Remained length: | Initial length: | Length fraction: " << endl;
	lengthTotalGlobal = 0;
	removedTotalGlobal = 0;
	for (int i = 0; i < localSearch -> data -> scaffolds.size(); i++){
		Scaffold * currentScaffold = localSearch -> data -> scaffolds[i];
		vector<int> data;
		int numberOfRemoved = 0;
		int numberOfPresent = 0;
		int length = 0;
		int deletedLength = 0;
		for (int j = 0; j < currentScaffold -> scaffold.size(); j++){
			Contig * contig = currentScaffold -> scaffold[j];
			if ((contig -> isDeleted()) == false){
				numberOfPresent++;
				length = length + contig->length;
				lengthTotalGlobal += contig->length;
			}
			else{
				numberOfRemoved++;
				deletedLength += contig->length;
				lengthTotalGlobal += contig->length;
				removedTotalGlobal += contig->length;
			}
		}
		
		cout << setw(15) << i + 1 << setw(15) << numberOfRemoved << setw(20) << length << setw(20) << deletedLength + length << setw(20) << (double(length)/(deletedLength + length))<< endl;
		basicStats << setw(15) << i + 1 << setw(15) << numberOfRemoved << setw(20) << length << setw(20) << deletedLength + length << setw(20) << (double(length)/(deletedLength + length))<< endl;
		
		removedTotal += numberOfRemoved;
		data.push_back(numberOfRemoved);
		data.push_back(numberOfPresent);
		data.push_back(length);
		data.reserve(3);
		scaffoldInfo.insert(make_pair(i+1, data));
	}
	basicStats.close();
	
}

int ifHasKey(int id){
	pos = scaffoldInfo.find(id);
	if (pos != scaffoldInfo.end())
		return 1;
	return 0;
}

/*
 * Function used to output newly generated scaffold file.
 * All the contigs with their flag set to deleted are
 * skipped during this process.
 * */

void outputScaffoldFile(const char * fileName, LocalSearch * localSearch){
	int scaffoldOrder = 1;
	outputScaffFile.open(fileName);
	getStatistics(localSearch);
	cout << "SCAFFOLD ID" << "\t" << "NUM. OF CONTIGS" << endl;
	for (int i = 0; i < localSearch -> data -> scaffolds.size(); i++){
		Scaffold * currentScaffold = localSearch -> data -> scaffolds[i];
		outputScaffFile << ">scaffold_" << scaffoldOrder << endl;
		int numberOfContigs = 0;
		for (int j = 0; j < currentScaffold -> scaffold.size(); j++){
			Contig * contig = currentScaffold -> scaffold[j];
			if ((contig -> isDeleted()) == false){
				outputScaffFile << contig->name << "\t" << contig->orientation << "\t" << contig->length << "\t" << contig->rightGap << endl;
				numberOfContigs++;
			}
		}
		
		
		cout << setw(10) << scaffoldOrder << setw(10) << numberOfContigs << endl;
		
		scaffoldOrder ++;
		int removed = 0;
		for (int j = 0; j < currentScaffold -> scaffold.size(); j++){
			Contig * contig = currentScaffold -> scaffold[j];
			if ((contig -> isDeleted()) == true){
				outputScaffFile << ">scaffold_" << scaffoldOrder << endl;
				outputScaffFile << contig->name << "\t" << contig->orientation << "\t" << contig->length << "\t" << contig->rightGap << endl;
				cout << setw(10) << scaffoldOrder << setw(10) << 1 << endl;
				scaffoldOrder++;
				removed++;
			}
		}
	}
	outputScaffFile.close();
}

/*
 * Function which maps info from contig file with the actual contig.
 * Info is stored inside the map which will be used for the final
 * fasta file generation. (output.fasta)
 * */

void generateFastaFile(const char * fileName){
	string line;
	string currentContig;
	ifstream contigFile(fileName);
	if (!contigFile){
		cerr << "Can't open contig file, exiting..." << endl;
		exit(1);
	}
	cout << "Generating fasta file..." << endl;
	while (getline(contigFile, line)){
		if (line[0] == '>'){
			line.erase(line.begin());
			vector<string> tokens;
			boost::algorithm::split(tokens, line, boost::algorithm::is_any_of(" "));
			currentContig = tokens[0];
			string data = "";
			contigMapper.insert(make_pair(currentContig, data));
		}
		else{
			if (line.size() > 0){
				line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());
				contigMapper[currentContig] += line;
			}
		}
	}
	contigFile.close();
	
	unordered_map<string, string>::iterator poz;
}

/*
 * Simple function used to produce gaps.
 * Each gap is marked with certain number of Ns.
 * If the gap size is negative, we put 10 Ns.
 * */

string produceGaps(int gapSize){
	string blank = "";
	if (gapSize > 0){
	for (int i = 0; i < gapSize; i++){
		blank = blank + "N";
		}
	}
	else if (gapSize < 10){
		blank = "NNNNNNNNNN";
	}
	return blank;
}

/*
 * Function used to get the reverse complement of
 * the input sequence.
 * */

string reverseComplementContig(string contig){
	unordered_map<char, char> complements;
	complements.insert(make_pair('A','T'));
	complements.insert(make_pair('T','A'));
	complements.insert(make_pair('G','C'));
	complements.insert(make_pair('C','G'));
	string reversedContig = string(contig.rbegin(), contig.rend());
	for (int i = 0; i < reversedContig.size(); i++){
		reversedContig[i] = complements[reversedContig[i]];
	}
	return reversedContig;
}

/*
 * True final fasta file generation. Fasta file is stored inside
 * the output folder under the name output.fasta.
 * */

void mappContigsWithScaffoldData(const char * fileName, const char * fastaName){
	inputScaffFile.open(fileName);
	fastaOutput.open(fastaName);
	string line;
	while (getline(inputScaffFile, line)){
		if (line[0] == '>'){
			fastaOutput << line << endl;
		}
		else{
			vector<string> tokens;
			boost::algorithm::split(tokens, line, boost::algorithm::is_any_of("\t"));
			int gapSize = boost::lexical_cast<int>(tokens[3]);
			string orientation = tokens[1];
			string contig = contigMapper[tokens[0]];
			if (orientation == "EB"){
				contig = reverseComplementContig(contig);
			}
			string scaffold = contig + produceGaps(gapSize);
			fastaOutput << scaffold << endl;
			
		}
	}
	inputScaffFile.close();
	fastaOutput.close();
}

/*
 * System call to preprocessor module to create super-contigs.
 * */

void createSuperContigs(const char * fileName, const char * contigFileName, const char * scaffoldFileName, const char * clusterFileName, int thrsh)
{
	string argument1 = contigFileName;
	string argument2 = scaffoldFileName;
	string argument3 = clusterFileName;
	string num = boost::lexical_cast<string>(thrsh);
	string cmd = "python src/preprocessor/preprocessor.py "+ argument3 + " " + argument1 + " " + argument2 + " " + num; 
	int returnValue = system(cmd.c_str());
	if (returnValue != 0){
		cout << "Supercontigs creation failed. Quiting..." << endl;
		exit(-1);
	}
}

/*
 * System call to preprocessor module to reconstruct the merged regions.
 * */

void returnMergedRegionsToScaffolds(){
	int returnValue = system("python src/preprocessor/reconstructor.py");
	if (returnValue != 0){
                cout << "Supercontigs creation failed. Quiting..." << endl;
                exit(-1);
        }

}

void rescaffold(const char * fileName, const char * contigFileName, const char * scaffoldFileName, const char * clusterFileName, const char * libFileName, int thrsh){
	LocalSearch * localSearch;
	if (thrsh == -1000){
		//createSuperContigs(fileName, contigFileName, scaffoldFileName, clusterFileName,  thrsh);
		cout << "No threshold for merging, supercontig creation skipped." << endl;
		localSearch = new LocalSearch(clusterFileName, scaffoldFileName, libFileName);
	}
	else{
		createSuperContigs(fileName, contigFileName, scaffoldFileName, clusterFileName,  thrsh);
		localSearch = new LocalSearch("src/preprocessor/all_edges.updated.cluster", "src/preprocessor/scaffolds_merged.scaf", libFileName);
}
	outputScaffoldFile(fileName, localSearch);
	generateFastaFile(contigFileName);
	if (thrsh != -1000){
		returnMergedRegionsToScaffolds();
		mappContigsWithScaffoldData("src/preprocessor/res_scaffold.final.scaf", "output/output.fasta");
	}
	else
		mappContigsWithScaffoldData("src/res_scaffolds.scaf", "output/output.fasta");
	cout << "------------------------------------------------" << endl;
	cout << "Summary:" << endl;
	cout << "\tNUMBER OF AMBIGUOUS REGIONS DETECTED: " << localSearch -> numberOfAmbiguousRegions << endl;
        cout << "\tNUMBER OF INITIALLY UNVALID EDGES: " << localSearch -> numberOfInitialUnhappyEdges << endl;
	cout << "\tREMOVED TOTAL: " << removedTotal << " contigs." << endl;
	cout << "\tPERCENTAGE OF LENGTH REMOVED: " << (long double) removedTotalGlobal / lengthTotalGlobal * 100 << "%" << endl;
	delete localSearch;
}


	



