#ifndef SUB_SCAFFOLD_H
#define SUB_SCAFFOLD_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>

#include "contig.h"

using namespace std;

/*
 * SubScaffold - class that mimics real partial scaffold.
 * It contains the vector of pointers to Contigs that
 * belong to the current partial scaffold and methods
 * that enable inserting contigs to partial scaffold,
 * marking contigs as borders and so on.
 * */

class SubScaffold{
	public:
	int depth;
	int numberOfUnhappyEdges;
	int numberOfDeleted;
	vector<Contig *> innerContigs;
	vector<int> rightGapSizes;
	Contig * borderContig;
	Contig * borderContigEnd;
	
	SubScaffold(){
		depth = 0;
		numberOfUnhappyEdges = 0;
		numberOfDeleted = 0;
		borderContig = NULL;
		borderContigEnd = NULL;
	}
	~SubScaffold(){
		//delete borderContig;
		//delete borderContigEnd;
	}
	
	SubScaffold(const SubScaffold & ref){
		depth = ref.depth + 1;
		borderContig = ref.borderContig;
		borderContigEnd = ref.borderContigEnd;
		numberOfUnhappyEdges = ref.numberOfUnhappyEdges;
		numberOfDeleted = 0;
		for (int i = 0; i < ref.rightGapSizes.size(); i++)
			rightGapSizes.push_back(rightGapSizes[i]);
		for (vector<Contig *>::const_iterator it = ref.innerContigs.begin(); it != ref.innerContigs.end(); it++){
			innerContigs.push_back(*it);
		}
	}
	
	void deleteAllContigs(){
		for (vector<Contig *>::const_iterator it = innerContigs.begin(); it != innerContigs.end(); it++){
			delete *it;
		}
		innerContigs.clear();
	}
	
	void addInnerContig(Contig * contig){
		innerContigs.push_back(contig);
	}
	
	void insertInnerGap(int gap){
		rightGapSizes.push_back(gap);
	}
	
	void printInnerContigs(){
		for (int i = 0; i < innerContigs.size(); i++){
			cout <<  &(innerContigs[i]) -> name << endl;
			cout << (innerContigs[i]) -> name << endl;
		}
	}
	
	vector<Contig *> * getInnerContigs(){
		return &(innerContigs);
	}
	
	void setBorderContig(Contig * contig){
		borderContig = contig;
	}
	
	void setBorderContigEnd(Contig * contig){
		borderContigEnd = contig;
	}
	
	int getSizeOfInnerContigs(){
		return (innerContigs.size());
	}
	
	void incrementNumberOfUnhappyEdges(){
		numberOfUnhappyEdges++;
	}
	
	int getNumberOfUnhappyEdges(){
		return numberOfUnhappyEdges;
	}

	void incrementNumberOfDeletedContigs(){
		numberOfDeleted ++;
	}

	int getNumberOfDeletedContigs(){
		return numberOfDeleted;
	}
	
};

#endif /*LOCAL_SEARCH_H*/
