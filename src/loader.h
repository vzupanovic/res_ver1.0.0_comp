#ifndef LOADER_H
#define LOADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>
#include <unordered_map>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

#include "scaffold1.h"
#include "parser.h"

/*
 * Loader - class for loading scaffold file into the vector of Scaffolds.
 * Class contains methods for constructing partial scaffold
 * by breaking initial scaffolds on each border contig.
 * Created partial scaffolds are assigned to adequate scaffolds
 * from which they originated.
 * */

class Loader{
	public:
	vector<Scaffold *> scaffolds;
	Loader(){
		
	}
	
	~Loader(){
		for (vector<Scaffold *>::const_iterator it = scaffolds.begin(); it != scaffolds.end(); it++){
			delete *it;
		}
	}
	
	
	void loadData(const char * fileName){
		string contigData;
		ifstream inFile(fileName);
		if (!inFile){
			cerr << "Can't open scaffold file, exiting..." << endl;
			exit(1);
		}
		string header; //skip header line
		getline(inFile, header);
		int idCounter = 0;
		int scaffoldNum = -1;
		bool inScaffold = false;
		while (getline(inFile, contigData)){
			vector<string> tokens;
			boost::algorithm::split(tokens, contigData, boost::algorithm::is_any_of("\t"));
			string contigName, contigOrientation;
			int contigLength, gapSize, ID;
			if (contigData[0] != '>'){
				if (inScaffold == false){
					inScaffold = true;
					Scaffold * _scaffold = new Scaffold(scaffoldNum);
					//cout << "adresa: " << _scaffold << " " << sizeof(_scaffold) << endl;
					scaffolds.push_back(_scaffold);
					scaffoldNum++;
					idCounter = 0;
				}
				contigName = tokens[0];
				contigOrientation = tokens[1];
				contigLength = boost::lexical_cast<int>(tokens[2]);
				gapSize = boost::lexical_cast<int>(tokens[3]);
				ID = idCounter;
				Contig * contig = new Contig(contigName,contigOrientation,contigLength, gapSize, idCounter);
				idCounter ++;
				Scaffold * _scaffold = scaffolds[scaffoldNum];
				scaffolds[scaffoldNum]-> addContigToScaffold(contig);
				contig -> assignScaffoldToContig(&(_scaffold -> id));
			}
			else{
				inScaffold = false;
			}
		}
	}
	
	void printData(){
		for (int i = 0; i < scaffolds.size(); i++){
			Scaffold * currentScaffold = scaffolds[i];
			cout << "SCAFFOLD: " << i << endl;
			vector<Contig *> pom = scaffolds[i] ->scaffold;
			for (int j = 0; j < pom.size(); j ++)
				cout << pom[j] ->name << endl;
		}
	}
	
	void iterateAndConstructPartialScaffolds(const char * libFileName){
		for (int i = 0; i < scaffolds.size(); i++){
			Scaffold * currentScaffold = scaffolds[i];
			constructPartialScaffolds(currentScaffold, libFileName);
		}
	}
	
	void constructPartialScaffolds(Scaffold * _scaffold, const char * libFileName){
		int mean;
		int stDev;
		int borderSum;
		Parser parser(libFileName, mean, stDev, borderSum);
		Contig * currentBorder = NULL;
		int currentSub = -1;
		for (int i = 0; i < _scaffold -> scaffold.size(); i++){
			if (((_scaffold -> scaffold[i]) -> length) >= borderSum){
				SubScaffold * subscaffold = new SubScaffold();
				if (currentSub >= 0){
					(_scaffold->partialScaffold[currentSub])->setBorderContigEnd(_scaffold -> scaffold[i]);
				}
				currentBorder = _scaffold -> scaffold[i];
				subscaffold -> setBorderContig(currentBorder);
				_scaffold->scaffold[i]->markContigAsBorder();
				_scaffold ->addPartialScaffold(subscaffold);
				currentSub ++;
				
			}
			
			else if (currentBorder != NULL){
				Contig * smallContig = _scaffold -> scaffold[i];
				(_scaffold ->partialScaffold[currentSub])->addInnerContig(smallContig);
			}
		}
		
	}
	
	void printAllBorderContigs(){
		for (int i = 0; i < scaffolds.size(); i++){
			Scaffold * currentScaffold = scaffolds[i];
			for (int j = 0; j < currentScaffold->scaffold.size(); j++){
				if ((currentScaffold->scaffold[j]->border) == 1)
					cout << "Border: " << currentScaffold->scaffold[j]->name << endl;
			}
			}
				
	}
	
	void printContigBuckets(){
		for (int i = 0; i < scaffolds.size(); i++){
			Scaffold * currentScaffold = scaffolds[i];
			cout << "SCAFFOLD: " << i << endl;
			vector<SubScaffold *> pom = currentScaffold ->partialScaffold;
			for (int j = 0; j < pom.size(); j++){
			cout << "border: " << pom[j]->borderContig->name << endl;	
				vector<Contig *> pomInnerCon = pom[j]->innerContigs;
				for (int k = 0; k < pomInnerCon.size(); k++){
					cout << "Inner: " << pomInnerCon[k] -> name << endl;
				}
			}
				
		}
	}
	
	void connectContigsWithPartialScaffolds(){
		int globalSubscaffoldId = 0;
		for (int i = 0; i < scaffolds.size(); i++){
			Scaffold * currentScaffold = scaffolds[i];
			vector<SubScaffold *> pom = currentScaffold ->partialScaffold;
			for (int j = 0; j < pom.size(); j++){
				int innerLength = pom[j]->innerContigs.size();
				pom[j]->borderContig->assignSubScaffoldToContig(globalSubscaffoldId);
				vector<Contig *> pomInnerCon = pom[j]->innerContigs;
				for (int k = 0; k < pomInnerCon.size(); k++){
					pomInnerCon[k] -> assignSubScaffoldToContig(globalSubscaffoldId);
				}
				globalSubscaffoldId = globalSubscaffoldId + 1;
			}
				
		}
	}
	
};

#endif /*LOADER_H*/
