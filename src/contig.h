#ifndef CONTIG_H
#define CONTIG_H

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

/*
 * Contig - the most important structure in the code.
 * All the informations about each contig are stored inside
 * this strucure (contig length, right gap size, deleted
 * flag, visible flag and so on.) 
 * Only this structure is trully allocated. All the others
 * contain pointers to this one. 
 * */

struct SubScaffoldID{
		int partID1;
		int partID2;
};

class Contig{
	public:
	string name;
	string orientation;
	int length;
	int rightGap;
	int id;
	int *scaffoldID;
	int matrixId;
	int edgeMatrixID;
	int border;
	int subscaffoldID;
	bool deleted;
	bool visible;
	
	Contig(string n, string o, int l, int rg, int ID){
		name = n;
		orientation = o;
		length = l;
		rightGap = rg;
		id = ID;
		scaffoldID = NULL;
		matrixId = 0;
		border = 0;
		subscaffoldID = -1;
		edgeMatrixID = -1;
		deleted = false;
		visible = false;
	}
	
	void assignScaffoldToContig(int * sID){
		scaffoldID = sID;
	}
	
	void assignSubScaffoldToContig(int sID){
		subscaffoldID = sID;
	}
	
	void assignMatrixId(int id){
		matrixId = id;
	}
	
	void assignEdgeMatrixId(int id){
		edgeMatrixID = id;
	}
	
	void markContigAsBorder(){
		border = 1;
	}
	
	int getMatrixId(){
		return matrixId;
	}
	
	int getEdgeMatrixId(){
		return edgeMatrixID;
	}
	
	void setDeleted(){
		deleted = true;
		visible = false;
	}
	
	bool isDeleted(){
		return deleted;
	}
	
	void updateGapSize(int gapSize){
		rightGap = gapSize;
	}
	
	void setInvisible(){
		visible = false;
	}
	
	void setVisible(){
		visible = true;
	}

	bool isVisible(){
		return visible;
	}
};

#endif /*CONTIG_H*/
